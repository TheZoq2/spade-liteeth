`default_nettype none

module pll_30_to_125
(
    input clk30,
    output output__,
);
  wire[1:0] clocks;
  ecp5pll
  #(
      .in_hz(30000000),
    .out0_hz(125000000),
  )
  ecp5pll_inst
  (
    .clk_i(clk30),
    .clk_o(clocks),
  );

  assign output__ = clocks[0];
endmodule

