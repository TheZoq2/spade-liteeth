#
# This file is heavily inspired by gen.py of LiteDRAM.
#
# Original copyright
#
# Copyright (c) 2018-2021 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2020 Stefan Schrijvers <ximin@ximinity.net>
# SPDX-License-Identifier: BSD-2-Clause

from typing import Any, Tuple, Dict
import yaml
import argparse
import pathlib
import os

class Port:
    def __init__(
        self,
        # The port defintiion of the #[no_mangle] entity to be generated
        spade_stub_head,
        # Extra code in the body of the spade module to map ports to
        # spade struct
        spade_body,
        # Bindings from the things defined in spade_body to the stub
        spade_bindings,
        # Port binding to return from the module
        spade_return_port,
        # Output port definitions of the spade module
        spade_output_type,
    ) -> None:
        self.spade_stub_head = spade_stub_head
        self.spade_body = spade_body
        self.spade_bindings = spade_bindings
        self.spade_output_type = spade_output_type
        self.spade_return_port = spade_return_port

def generate_streamer_port(port: Tuple[str, Dict[str, Any]]) -> Port:
    (port_name, props) = port

    spade_stub_head = "\n    ".join([
        f"#[no_mangle] {port_name}_ip_address: uint<32>,",
        f"#[no_mangle] {port_name}_sink_data: uint<32>,",
        f"#[no_mangle] {port_name}_sink_last: bool,",
        f"#[no_mangle] {port_name}_sink_ready: &mut bool,",
        f"#[no_mangle] {port_name}_sink_valid: bool,",
        f"#[no_mangle] {port_name}_source_data: &mut uint<32>,",
        f"#[no_mangle] {port_name}_source_error: &mut bool,",
        f"#[no_mangle] {port_name}_source_last: &mut bool,",
        f"#[no_mangle] {port_name}_source_ready: bool,",
        f"#[no_mangle] {port_name}_source_valid: &mut bool,",
        f"#[no_mangle] {port_name}_udp_port: uint<16>,",
    ])
    spade_body = "\n    ".join([
        f"let ({port_name}, {port_name}_inv) = port;",
    ])
    spade_bindings = "\n        ".join([
        f"{port_name}_ip_address: *{port_name}_inv.ip_address,",
        f"{port_name}_sink_data: *{port_name}_inv.sink_data,",
        f"{port_name}_sink_last: *{port_name}_inv.sink_last,",
        f"{port_name}_sink_ready: {port_name}_inv.sink_ready,",
        f"{port_name}_sink_valid: *{port_name}_inv.sink_valid,",
        f"{port_name}_source_data: {port_name}_inv.source_data,",
        f"{port_name}_source_error: {port_name}_inv.source_error,",
        f"{port_name}_source_last: {port_name}_inv.source_last,",
        f"{port_name}_source_ready: *{port_name}_inv.source_ready,",
        f"{port_name}_source_valid: {port_name}_inv.source_valid,",
        f"{port_name}_udp_port: *{port_name}_inv.udp_port,",
    ])
    spade_return_port = f"{port_name}"

    return Port(
        spade_stub_head=spade_stub_head,
        spade_body=spade_body,
        spade_bindings=spade_bindings,
        spade_return_port=spade_return_port,
        spade_output_type="LiteEthStream"
    )
    

def generate_raw_port(port: Tuple[str, Dict[str, Any]]) -> Port:
    (port_name, props) = port

    spade_stub_head = "\n    ".join([
        f"#[no_mangle] {port_name}_sink_data: uint<32>,",
        f"#[no_mangle] {port_name}_sink_dst_port: uint<16>,",
        f"#[no_mangle] {port_name}_sink_ip_address: uint<32>,",
        f"#[no_mangle] {port_name}_sink_last: bool,",
        f"#[no_mangle] {port_name}_sink_last_be: [bool; 4],",
        f"#[no_mangle] {port_name}_sink_length: uint<16>,",
        f"#[no_mangle] {port_name}_sink_ready: &mut bool,",
        f"#[no_mangle] {port_name}_sink_src_port: uint<16>,",
        f"#[no_mangle] {port_name}_sink_valid: bool,",
        f"#[no_mangle] {port_name}_source_data: &mut uint<32>,",
        f"#[no_mangle] {port_name}_source_dst_port: &mut uint<16>,"
        f"#[no_mangle] {port_name}_source_error: &mut bool,",
        f"#[no_mangle] {port_name}_source_ip_address: &mut uint<32>,",
        f"#[no_mangle] {port_name}_source_last: &mut bool,",
        f"#[no_mangle] {port_name}_source_last_be: &mut [bool; 4],",
        f"#[no_mangle] {port_name}_source_length: &mut uint<16>,",
        f"#[no_mangle] {port_name}_source_ready: bool,",
        f"#[no_mangle] {port_name}_source_src_port: &mut uint<16>,",
        f"#[no_mangle] {port_name}_source_valid: &mut bool,",
    ])

    spade_body = "\n    ".join([
        f"let ({port_name}, {port_name}_inv) = port;",
    ])
    spade_bindings = "\n        ".join([
        f"{port_name}_sink_data: *{port_name}_inv.sink_data,",
        f"{port_name}_sink_dst_port: *{port_name}_inv.sink_dst_port,",
        f"{port_name}_sink_ip_address: *{port_name}_inv.sink_ip_address,",
        f"{port_name}_sink_last: *{port_name}_inv.sink_last,",
        f"{port_name}_sink_last_be: *{port_name}_inv.sink_last_be,",
        f"{port_name}_sink_length: *{port_name}_inv.sink_length,",
        f"{port_name}_sink_ready: {port_name}_inv.sink_ready,",
        f"{port_name}_sink_src_port: *{port_name}_inv.sink_src_port,",
        f"{port_name}_sink_valid: *{port_name}_inv.sink_valid,",
        f"{port_name}_source_data: {port_name}_inv.source_data,",
        f"{port_name}_source_dst_port: {port_name}_inv.source_dst_port,"
        f"{port_name}_source_error: {port_name}_inv.source_error,",
        f"{port_name}_source_ip_address: {port_name}_inv.source_ip_address,",
        f"{port_name}_source_last: {port_name}_inv.source_last,",
        f"{port_name}_source_last_be: {port_name}_inv.source_last_be,",
        f"{port_name}_source_length: {port_name}_inv.source_length,",
        f"{port_name}_source_ready: *{port_name}_inv.source_ready,",
        f"{port_name}_source_src_port: {port_name}_inv.source_src_port,",
        f"{port_name}_source_valid: {port_name}_inv.source_valid,",
    ])
    spade_return_port = f"{port_name}"

    return Port(
        spade_stub_head=spade_stub_head,
        spade_body=spade_body,
        spade_bindings=spade_bindings,
        spade_return_port=spade_return_port,
        spade_output_type="LiteEthRaw"
    )

def generate_port(port: Tuple[str, Dict[str, Any]]) -> Port:
    (_, props) = port
    match props["mode"]:
        case "streamer":
            return generate_streamer_port(port)
        case "raw":
            return generate_raw_port(port)
        case other:
            raise ValueError(f"Port of type {other} is unsupported")

def generate_files(
    config: Dict[str, Dict[str, Any]],
    spade_stub_file: pathlib.Path
) -> str:
    ports = [generate_port(port) for port in config["udp_ports"].items()]

    with open(spade_stub_file, 'r') as f:
        spade_stub = f.read()

    spade_stub_inputs = " ".join([port.spade_stub_head for port in ports])
    spade_body = "\n".join([port.spade_body for port in ports])
    spade_bindings = "\n".join([port.spade_bindings for port in ports])
    if len(ports) == 1:
        spade_types = ports[0].spade_output_type
        spade_return_port = ports[0].spade_return_port
    else:
        spade_types = ", ".join([port.spade_output_type for port in ports])
        spade_return_port = ", ".join([port.spade_return_port for port in ports])

    sys_clk_freq = config["clk_freq"];
    main_clk = f"clk_{sys_clk_freq}"


    return (
        spade_stub
            .replace("##SYS_CLK##", main_clk)
            .replace("##STUB_INPUTS##", spade_stub_inputs)
            .replace("##STREAMS##", spade_types)
            .replace("##BODY##", spade_body)
            .replace("##BINDINGS##", spade_bindings)
            .replace("##RETURN_PORTS##", spade_return_port)
    )


def main():
    parser = argparse.ArgumentParser(description="LiteETH spade stub generator")
    parser.add_argument("--spade_outdir")
    parser.add_argument("config", help="YAML config file")
    args = parser.parse_args()


    self_dir = pathlib.Path(__file__).parent.resolve()

    config_file = pathlib.Path(args.config)
    spade_stub = self_dir / "stubs" / "spade_stub.spade"

    spade_stub.stat().st_mtime

    out_spade = pathlib.Path(args.spade_outdir) / "liteeth.spade"

    # Check if any files need rebuilding
    targets_exists =  out_spade.exists()

    config = yaml.load(open(args.config).read(), Loader=yaml.Loader)

    spade = generate_files(config, spade_stub)

    if not os.path.exists(args.spade_outdir):
        os.mkdir(pathlib.Path(args.spade_outdir))

    with open(out_spade, 'w') as f:
        f.write(spade)

if __name__ == "__main__":
    main()
